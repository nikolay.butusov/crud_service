import uvicorn
from fastapi import FastAPI
from src.users_router import users_router
from src.products_router import products_router
from src.orders_router import orders_router


myCrud = FastAPI()



@myCrud.get("/")
async def homePage():
     return 'Welcome to CRUD-service'


myCrud.include_router(users_router, prefix="/users", tags=["users"])

myCrud.include_router(products_router, prefix="/products", tags=["products"])

myCrud.include_router(orders_router, prefix="/orders", tags=["orders"])

if __name__ == "__main__":
    uvicorn.run(myCrud, host="127.0.0.1", port=8080)
