from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy import URL, create_engine, text


async_engine = create_async_engine("postgresql+asyncpg://nickbut:11223344@172.22.0.1:5440/db", echo=False)

async_session_factory = async_sessionmaker(async_engine)

