from fastapi import APIRouter
from sqlalchemy import select

# from src.database import session_factory
# from src.models import Products
# from src.logger import logger

from src.database import async_session_factory
from src.models import Products
from src.logger import logger


products_router = APIRouter()


# ----------Эндпоинты для Products-------------

# Список всех продуктов
@products_router.get("")
async def get_all_products():
    async with async_session_factory() as session:
        query = select(Products)
        result = await session.execute(query)
        logger.info(f"IN_GET_ALL_PRODUCTS: All products returned")
        return result.scalars().all()

# Продукт по id
@products_router.get("/{product_id}")
async def get_product(product_id:int):
    async with async_session_factory() as session:
        product = await session.get(Products, product_id)
        if product:
            logger.info(f"IN_GET_PRODUCT: Product with p_id = {product_id} FOUND")
            return product
        logger.warning(f"IN_GET_PRODUCT: Product with p_id = {product_id} NOT FOUND")
        return "No such product"

# Добавление нового продукта
@products_router.post("")
async def add_product(product_name: str, description: str, price: int):
    async with async_session_factory() as session:
        new_product = Products(product_name=product_name, description=description, price=price)
        session.add(new_product)
        await session.commit()
        logger.info(f"IN_ADD_PRODUCT: New product added")
        return "Product added"


# Изменение данных о продукте по id
@products_router.put("/{product_id}")
async def update_product(product_id: int, product_name: str = "", description: str = "", price: int = -1):
    async with async_session_factory() as session:
        product = await session.get(Products, product_id)
        if product:
            if product_name != "":
                product.product_name = product_name
            if description != "":
                product.description = description
            if price != -1:
                product.price = price
            await session.commit()
        else:
            logger.warning(f"IN_UPDATE_PRODUCT: Product with p_id = {product_id} NOT FOUND")
            return "No such product"
        logger.info(f"IN_UPDATE_PRODUCT: Product with p_id = {product_id} UPDATED")
        return "Product updated"


# Удаление продукта по id
@products_router.delete("/{product_id}")
async def delete_product(product_id: int):
    async with async_session_factory() as session:
        product = await session.get(Products, product_id)
        if product:
            session.delete(product)
            await session.commit()
            logger.info(f"IN_DELETE_PRODUCT: Product with p_id = {product_id} DELETED")
            return "Product deleted"
        else:
            logger.waning(f"IN_DELETE_PRODUCT: Product with p_id = {product_id} NOT FOUND")
            return "No such product"
