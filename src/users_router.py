from fastapi import APIRouter
from sqlalchemy import select

# from src.database import session_factory
# from src.models import Users
# from src.logger import logger

from src.database import async_session_factory
from src.models import Users
from src.logger import logger


users_router = APIRouter()


@users_router.get("")
async def get_all_users():
    async with async_session_factory() as session:
        query = select(Users)
        result = await session.execute(query)
        logger.info(f"IN_GET_ALL_USERS: All users returned")
        return result.scalars().all()


@users_router.get("/{user_id}")
async def get_user(user_id:int):
    async with async_session_factory() as session:
        user = await session.get(Users, user_id)
        if user:
            logger.info(f"IN_GET_USER: User with u_id = {user_id} FOUND")
            return user
        logger.warning(f"IN_GET_USER: User with u_id = {user_id} NOT FOUND")
        return "No such user"


@users_router.post("")
async def add_user(first_name: str, last_name: str, address: str, email: str):
    async with async_session_factory() as session:
        new_user = Users(first_name=first_name, last_name=last_name, address=address, email=email)
        session.add(new_user)
        await session.commit()
        logger.info(f"IN_ADD_USER: New user added")
        return "User added"


@users_router.put("/{user_id}")
async def update_user(user_id: int, first_name: str = "", last_name: str = "", address: str = "", email: str = ""):
    async with async_session_factory() as session:
        user = await session.get(Users, user_id)
        if user:
            if first_name != "":
                user.first_name = first_name
            if last_name != "":
                user.last_name = last_name
            if address != "":
                user.address = address
            if email != "":
                user.email = email
            await session.commit()
        else:
            logger.warning(f"IN_UPDATE_USER: User with u_id = {user_id} NOT FOUND")
            return "No such user"
        logger.info(f"IN_UPDATE_USER: User with u_id = {user_id} UPDATED")
        return "User updated"


@users_router.delete("/{user_id}")
async def delete_user(user_id: int):
    async with async_session_factory() as session:
        user = await session.get(Users, user_id)
        if user:
            session.delete(user)
            await session.commit()
            logger.info(f"IN_DELETE_USER: User with u_id = {user_id} DELETED")
            return "User deleted"
        else:
            logger.waning(f"IN_DELETE_USER: User with u_id = {user_id} NOT FOUND")
            return "No such user"
