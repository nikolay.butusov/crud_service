from fastapi import APIRouter
from sqlalchemy import select
# from src.database import session_factory
# from src.models import Orders, Users, Products
# from src.logger import logger

from src.database import async_session_factory
from src.models import Orders, Users, Products
from src.logger import logger


orders_router = APIRouter()



# -------------Эндпоинты для Orders--------------

# Список всех заказов
@orders_router.get("")
async def get_all_orders():
    async with async_session_factory() as session:
        query = select(Orders)
        result = await session.execute(query)
        logger.info(f"IN_GET_ALL_ORDERS: All orders returned")
        return result.scalars().all()

# Заказ по id
@orders_router.get("/{order_id}")
async def get_orders(order_id:int):
    async with async_session_factory() as session:
        order = await session.get(Orders, order_id)
        if order:
            logger.info(f"IN_GET_ORDERS: Order with o_id = {order_id} FOUND")
            return order
        logger.warning(f"IN_GET_ORDERS: Order with o_id = {order_id} NOT FOUND")
        return "No such order"

# Добавление нового заказа
@orders_router.post("")
async def add_orders(user: int, product_ordered: int, total_paid: int):
    async with async_session_factory() as session:
        usr = await session.get(Users, user)
        prd = await session.get(Products, product_ordered)
        if usr and prd:
            new_order = Orders(user=user, product_ordered=product_ordered, total_paid=total_paid)
            session.add(new_order)
            await session.commit()
            logger.info(f"IN_ADD_ORDERS: New order added")
            return "Order added"
        elif not usr and prd:
            logger.warning(f"IN_ADD_ORDERS: User with u_id = {user} NOT FOUND")
            return "No such user"
        elif usr and not prd:
            logger.warning(f"IN_ADD_ORDERS: Product with p_id = {product_ordered} NOT FOUND")
            return "No such product"
        else:
            logger.warning(f"IN_ADD_ORDERS: User with u_id = {user} AND Product with p_id ={product_ordered} NOT FOUND")
            return "No such user and product"



# Изменение данных о заказе по id
@orders_router.put("/{order_id}")
async def update_orders(order_id: int, user: int = -1, product_ordered: int = -1, total_paid: int = -1):
    async with async_session_factory() as session:

        usr = await session.get(Users, user)
        prd = await session.get(Products, product_ordered)
        if usr and prd:
            order = await session.get(Orders, order_id)
            if order:
                if user != -1:
                    order.user = user
                if product_ordered != -1:
                    order.product_ordered = product_ordered
                if total_paid != -1:
                    order.total_paid = total_paid
                await session.commit()
            else:
                logger.warning(f"IN_UPDATE_ORDERS: Order with o_id = {order_id} NOT FOUND")
                return "No such order"
            logger.info(f"IN_UPDATE_ORDERS: Order with o_id = {order_id} UPDATED")
            return "Order updated"
        elif not usr and prd:
            logger.warning(f"IN_UPDATE_ORDERS: User with u_id = {user} NOT FOUND")
            return "No such user"
        elif usr and not prd:
            logger.warning(f"IN_UPDATE_ORDERS: Product with p_id = {product_ordered} NOT FOUND")
            return "No such product"
        else:
            logger.warning(f"IN_UPDATE_ORDERS: User with u_id={user} AND Product with p_id={product_ordered} NOT FOUND")
            return "No such user and product"


# Удаление заказа по id
@orders_router.delete("/{order_id}")
async def delete_orders(order_id: int):
    async with async_session_factory() as session:
        order = await session.get(Orders, order_id)
        if order:
            session.delete(order)
            await session.commit()
            logger.info(f"IN_DELETE_ORDERS: Order with o_id = {order_id} DELETED")
            return "Order deleted"
        else:
            logger.warning(f"IN_DELETE_ORDERS: Order with o_id = {order_id} NOT FOUND")
            return "No such Order"
